import React from "react";

import Index from "./src/containers/routes/Index";
import { Provider } from "react-redux";
import configureStore from "./src/config/";

const store = configureStore();

export default function App() {
  return (
    <Provider store={store}>
      <Index />
    </Provider>
  );
}
