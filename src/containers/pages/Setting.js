import React, { Component } from "react";
import { Text, StyleSheet, View, Image, Linking } from "react-native";
import { colors } from "../../styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class Setting extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headingContainer}>
          <Text style={styles.headingText}> Settings </Text>
          <View style={styles.userProfileContainer}>
            <Image
              source={require("../../assets/pizza.jpg")}
              style={styles.image}
            />
            <View style={styles.userInfo}>
              <View style={{ flexDirection: "column" }}>
                <Text style={styles.userName}>Muhamad Ibnu</Text>
                <Text style={styles.email}>Email</Text>
              </View>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(
                "mailto:rafli060395@gmail.com@gmail.com?subject=Halo&body=Halo salam kenal"
              );
            }}
          >
            <View style={styles.cardContainer}>
              <Text style={styles.textInfo}>Email Rafli Rachmawandi</Text>
              <Icon
                name="chevron-right"
                size={30}
                color={colors.greyLight}
                style={styles.icon}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(
                "mailto:ibnutriyuono23@gmail.com@gmail.com?subject=Halo&body=Halo salam kenal"
              );
            }}
          >
            <View style={styles.cardContainer}>
              <Text style={styles.textInfo}>Email Muhamad Ibnu Tri Yuono</Text>
              <Icon
                name="chevron-right"
                size={30}
                color={colors.greyLight}
                style={styles.icon}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.push("About")}>
            <View style={styles.cardContainer}>
              <Text style={styles.textInfo}>About</Text>
              <Icon
                name="chevron-right"
                size={30}
                color={colors.greyLight}
                style={styles.icon}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.logoutButtonContainer}
            onPress={() =>
              this.props.navigation.navigate("Login", {
                screen: "Login",
              })
            }
          >
            <Text style={styles.logoutText}>LOGOUT</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: colors.darkBg,
    justifyContent: "flex-start",
    flexDirection: "column",
    fontFamily: "Roboto",
  },
  headingContainer: {
    paddingTop: 35,
  },
  headingText: {
    color: "#FFF",
    fontSize: 42,
    fontWeight: "bold",
  },
  userProfileContainer: {
    flexDirection: "row",
    marginTop: 30,
  },
  image: {
    width: 80,
    height: 80,
    borderRadius: 150 / 2,
    overflow: "hidden",
    borderWidth: 3,
    borderColor: colors.greyLight,
    marginTop: 29,
  },
  userName: {
    color: colors.lightBg,
    fontSize: 24,
    fontWeight: "bold",
    marginLeft: 24,
  },
  email: {
    color: colors.greyLight,
    fontSize: 14,
    fontWeight: "bold",
    marginLeft: 24,
  },
  userInfo: {
    marginTop: 30,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  cardContainer: {
    marginTop: 29,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderColor: colors.greyLight,
    borderWidth: 2,
    borderRadius: 12,
    padding: 12,
    width: "100%",
  },
  textInfo: {
    fontSize: 13,
    color: colors.greyLight,
    marginLeft: 4,
  },
  icon: {},
  logoutButtonContainer: {
    marginTop: 29,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderColor: colors.dangerButton,
    borderWidth: 2,
    borderRadius: 12,
    padding: 12,
    width: 90,
  },
  logoutText: {
    fontSize: 13,
    color: colors.greyLight,
  },
});
