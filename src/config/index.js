import { createStore, combineReducers } from "redux";
import foodReducer from "./reducers";

const rootReducer = combineReducers({
  foodReducer: foodReducer,
});

const configureStore = () => createStore(rootReducer);

export default configureStore;
// const increment = () => {
//   return {
//     type: "INCREMENT",
//   };
// };

// const decrement = () => {
//   return {
//     type: "DECREMENT",
//   };
// };

// const counter = (state = 0, action) => {
//   switch (action.type) {
//     case "INCREMENT":
//       return state + 1;
//     case "DECREMENT":
//       return state - 1;
//   }
// };

// let store = createStore(counter);

// store.subscribe(() => console.log(store.getState));

// store.dispatch(increment());
