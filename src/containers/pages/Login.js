import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";
import { colors } from "../../styles/index";

export default class Login extends Component {
  state = {
    iconName: "eye-off",
    textShow: true,
  };

  handleEye() {
    if (this.state.iconName == "eye") {
      this.setState({
        iconName: "eye-off",
        textShow: true,
      });
    } else {
      this.setState({
        iconName: "eye",
        textShow: false,
      });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.kotak}>
          <Text> </Text>
        </View>
        <Text style={styles.wellcome}>Welcome!</Text>
        <Text style={styles.textSignIn}>Sign in to continue</Text>
        <View style={styles.inputContainer}>
          <Icon style={styles.personIcon} name="account" size={41} />
          <TextInput style={styles.textInput}></TextInput>
        </View>
        <View style={styles.inputContainer}>
          <Icon style={styles.lockIcon} name="lock" size={41} />
          <TextInput
            style={styles.textInput}
            secureTextEntry={this.state.textShow}
          ></TextInput>
          <Icon
            name={this.state.iconName}
            size={25}
            color={colors.greyLight}
            style={{ position: "absolute", right: 12 }}
            onPress={() => this.handleEye()}
          />
        </View>
        <TouchableOpacity
          style={styles.btnSignIn}
          onPress={() => this.props.navigation.push("App")}
        >
          <Text style={styles.signInText}>Sign in</Text>
          <Icon name="arrow-right" size={16} color="#ffffff" />
        </TouchableOpacity>
        <Text style={styles.forgot}>Forgot Password ?</Text>
        <TouchableOpacity style={styles.btnSignUp}>
          <Text
            style={styles.signUpText}
            onPress={() => this.props.navigation.push("Register")}
          >
            Create an account
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 12,
    backgroundColor: colors.darkBg,
    justifyContent: "center",
    flexDirection: "column",
    fontFamily: "Roboto",
  },
  kotak: {
    backgroundColor: colors.greenLight,
    width: 45,
    height: 45,
    borderRadius: 10,
    marginBottom: 15,
  },
  wellcome: {
    color: colors.lightBg,
    fontSize: 42,
    fontWeight: "bold",
  },
  textSignIn: {
    color: colors.greyLight,
    fontSize: 24,
  },
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
    width: "100%",
    overflow: "hidden",
  },
  textInput: {
    paddingLeft: 15,
    fontSize: 18,
    color: colors.greyLight,
    width: "90%",
    height: 54,
    borderBottomColor: "white",
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 4,
  },
  personIcon: {
    backgroundColor: colors.yellowDark,
    color: colors.yellowLight,
    marginTop: 6,
  },
  lockIcon: {
    backgroundColor: colors.redDark,
    color: colors.redLight,
    marginTop: 6,
  },
  btnSignIn: {
    marginVertical: 30,
    backgroundColor: colors.greenLight,
    height: 50,
    marginHorizontal: 5,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    flexDirection: "row",
  },
  signInText: {
    color: colors.lightBg,
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 16,
    marginRight: 5,
  },
  forgot: {
    fontSize: 14,
    fontFamily: "Roboto",
    fontWeight: "500",
    textAlign: "center",
    color: colors.greyLight,
  },
  btnSignUp: {
    marginTop: 35,
    backgroundColor: colors.greenDark,
    height: 50,
    marginHorizontal: 5,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    flexDirection: "row",
  },
  signUpText: {
    color: colors.greenLight,
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 16,
    marginRight: 5,
  },
});
