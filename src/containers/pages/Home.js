import React, { Component } from "react";
import {
  ActivityIndicator,
  Text,
  StyleSheet,
  View,
  Image,
  FlatList,
} from "react-native";
import { SearchBar } from "react-native-elements";
import { colors } from "../../styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {
  TouchableHighlight,
  TouchableOpacity,
} from "react-native-gesture-handler";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      data: [],
      page: 1,
      loading: false,
    };
  }

  updateSearch = (search) => {
    this.setState({ search });
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch(
      `https://api.nutritionix.com/v1_1/search/pizza?results=0:${this.state.page}&fields=item_name,brand_name,item_id,nf_calories,nf_sodium,nf_total_fat,nf_cholesterol&appId=96f3b741&appKey=0cf72cc7447fdf0afeaa2a108fa2926d`
    );
    const json = await response.json();
    // this.setState({ data: json.hits });
    this.setState((state) => ({
      data: [...state.data, ...json.hits],
      loading: false,
    }));
  };

  handleEnd = () => {
    this.setState(
      (state) => ({ page: state.page + 1 }),
      () => this.fetchData()
    );
  };

  render() {
    const { search, data } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.searchBar}>
          <SearchBar
            placeholder="Search..."
            lightTheme
            round
            onChangeText={this.updateSearch}
            value={search}
            containerStyle={styles.searhContainer}
            inputContainerStyle={styles.searchInput}
          />
          <TouchableOpacity>
            <Icon
              name="format-list-checkbox"
              size={50}
              color={colors.greyLight}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.categoryTab}>
          <TouchableHighlight style={styles.categoryBg}>
            <Text style={styles.categoryText}>Pizza</Text>
          </TouchableHighlight>
          <TouchableHighlight style={styles.categoryBg}>
            <Text style={styles.categoryText}>Pasta</Text>
          </TouchableHighlight>
          <TouchableHighlight style={styles.categoryBg}>
            <Text style={styles.categoryText}>Burger</Text>
          </TouchableHighlight>
          <TouchableHighlight style={styles.categoryBg}>
            <Text style={styles.categoryText}>Steak</Text>
          </TouchableHighlight>
        </View>

        <FlatList
          style={{ marginTop: 20 }}
          data={this.state.data}
          keyExtractor={(x, i) => i.toString()}
          onEndReached={() => this.handleEnd()}
          onEndReachedThreshold={0}
          ListFooterComponent={() =>
            this.state.loading ? null : (
              <ActivityIndicator size="large" animating />
            )
          }
          renderItem={({ item }) => (
            // <Text>{`${item.fields.item_name}`}</Text>
            <TouchableOpacity
              style={styles.card}
              onPress={() =>
                this.props.navigation.navigate("Detail", {
                  itemId: item.fields.item_id,
                })
              }
            >
              <Image
                source={require("../../assets/pizza.jpg")}
                style={styles.image}
              ></Image>
              <View style={styles.cardBody}>
                <Text
                  style={styles.cardTitle}
                >{`${item.fields.item_name}`}</Text>
                <Text style={styles.cardCategori}>Pizza</Text>
                <View style={styles.cardDesc}>
                  <Text>Total Fat</Text>
                  <Text>{`${item.fields.nf_total_fat} g`}</Text>
                </View>
                <View style={styles.cardDesc}>
                  <Text>Calories</Text>
                  <Text>{`${item.fields.nf_calories} cal`}</Text>
                </View>
                <View style={styles.cardDesc}>
                  <Text>Cholesterol</Text>
                  <Text>{`${item.fields.nf_cholesterol} mg`}</Text>
                </View>
                <View style={styles.cardDesc}>
                  <Text>Sodium</Text>
                  <Text>{`${item.fields.nf_sodium} mg`}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 12,
    backgroundColor: colors.darkBg,
    justifyContent: "flex-start",
    flexDirection: "column",
    fontFamily: "Roboto",
    paddingTop: 25,
  },
  searchBar: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  searhContainer: {
    backgroundColor: "transparent",
    borderWidth: 0,
    borderBottomColor: "transparent",
    borderTopColor: "transparent",
  },
  searchInput: {
    backgroundColor: colors.lightBg,
    borderRadius: 25,
    width: 300,
  },
  categoryTab: {
    flexDirection: "row",
    marginTop: 20,
  },
  categoryBg: {
    height: 75,
    width: 75,
    backgroundColor: colors.lightBg,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 100,
    marginLeft: 10,
  },
  categoryText: {
    color: colors.greyLight,
    fontFamily: "Roboto",
    fontSize: 18,
  },
  card: {
    backgroundColor: colors.lightBg,
    marginHorizontal: 15,
    borderRadius: 10,
    overflow: "hidden",
    marginBottom: 30,
  },
  image: {
    width: "100%",
    height: 145,
  },
  cardBody: {
    margin: 15,
  },
  cardTitle: {
    color: "#4F4F4F",
    fontSize: 24,
    fontFamily: "Roboto",
    fontWeight: "bold",
  },
  cardCategori: {
    color: colors.greyLight,
    fontFamily: "Roboto",
    marginBottom: 25,
  },
  cardDesc: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
