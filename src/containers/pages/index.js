import Login from "./Login";
import Register from "./Register";
import Home from "./Home";
import Detail from "./Detail";
import Profile from "./Profile";
import Setting from "./Setting";
import About from "./About";
import Favorite from "./Favorite";

export { Login, Register, Home, Detail, Profile, Setting, About, Favorite };
