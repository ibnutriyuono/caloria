import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  Image,
  ScrollView,
  Linking,
} from "react-native";
import { colors } from "../../styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class About extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headingContainer}></View>
        <Text style={styles.headingText}> About </Text>
        <View style={styles.profileContainer}>
          <ScrollView horizontal={true}>
            <View style={styles.cardCont}>
              <Image
                source={require("../../assets/xd.jpg")}
                style={styles.imageProfile}
              />
              <Text style={styles.usernameText}>Muhamad Ibnu</Text>
              <Text style={styles.emailText}>ibnutriyuono23@gmail.com</Text>
            </View>
            <View style={styles.cardCont}>
              <Image
                source={require("../../assets/rafli_rach.jpg")}
                style={styles.imageProfile}
              />
              <Text style={styles.usernameText}>Rafli Rachmawandi</Text>
              <Text style={styles.emailText}>rafli060395@gmail.com</Text>
            </View>
          </ScrollView>
        </View>
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <TouchableOpacity
            onPress={() => {
              Linking.openURL("https://gitlab.com/raflirach");
            }}
          >
            <View style={styles.logoutButtonContainer}>
              <Icon name="gitlab" size={20} color={colors.greyLight} />
              <Text style={styles.logoutText}>raflirach</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL("https://gitlab.com/ibnutriyuono");
            }}
          >
            <View style={styles.logoutButtonContainer}>
              <Icon name="gitlab" size={20} color={colors.greyLight} />
              <Text style={styles.logoutText}>ibnutriyuono</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL("https://www.linkedin.com/in/ibnutriyuono/");
            }}
          >
            <View style={styles.logoutButtonContainer}>
              <Icon name="linkedin" size={20} color={colors.greyLight} />
              <Text style={styles.logoutText}>ibnutriyuono</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL("https://www.instagram.com/ibnutriyuono/");
            }}
          >
            <View style={styles.logoutButtonContainer}>
              <Icon name="instagram" size={20} color={colors.greyLight} />
              <Text style={styles.logoutText}>ibnutriyuono</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL("https://www.instagram.com/rafli_rach/");
            }}
          >
            <View style={styles.logoutButtonContainer}>
              <Icon name="instagram" size={20} color={colors.greyLight} />
              <Text style={styles.logoutText}>rafli_rach</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: colors.darkBg,
    justifyContent: "flex-start",
    flexDirection: "column",
    fontFamily: "Roboto",
  },
  headingContainer: {
    paddingTop: 25,
  },
  headingText: {
    color: "#FFF",
    fontSize: 42,
    fontWeight: "bold",
  },
  profileContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  cardCont: {
    paddingRight: 20,
  },
  imageProfile: {
    width: 80,
    height: 80,
    borderRadius: 150 / 2,
    overflow: "hidden",
    borderWidth: 3,
    borderColor: colors.greyLight,
    marginLeft: 21,
    marginTop: 29,
  },
  usernameText: {
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 18,
    color: colors.lightBg,
    marginTop: 30,
  },
  emailText: {
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 18,
    color: colors.lightBg,
    marginTop: 9,
  },
  logoutButtonContainer: {
    flexDirection: "row",
    borderColor: colors.dangerButton,
    borderWidth: 2,
    borderRadius: 12,
    padding: 12,
    width: 135,
    marginBottom: 12,
  },
  logoutText: {
    fontSize: 13,
    color: colors.greyLight,
    paddingLeft: 12,
  },
});
