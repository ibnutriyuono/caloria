import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  Image,
  SafeAreaView,
  FlatList,
  ScrollView,
} from "react-native";
import { colors, fonts } from "../../styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { deleteFood, addFood } from "../../config/actions/";
import { ListItem } from "react-native-elements";

class Favorite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 1,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch(
      `https://api.nutritionix.com/v1_1/search/pizza?results=0:${this.state.page}&fields=item_name,brand_name,item_id,nf_calories,nf_sodium,nf_total_fat,nf_cholesterol&appId=96f3b741&appKey=0cf72cc7447fdf0afeaa2a108fa2926d`
    );
    const json = await response.json();
    // this.setState({ data: json.hits });
    this.setState({
      data: json.hits[0].fields,
    });
  };

  render() {
    const { data } = this.state;
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ flex: 1 }}>
          <View style={styles.headingContainer}>
            <Text style={styles.headingText}> Your {"\n"} Favorite </Text>
          </View>
          <View style={styles.bottomLine} />
          <View style={{ flexDirection: "row" }}>
            <ScrollView horizontal={true}>
              {this.props.foods.map((data, i) => (
                <TouchableOpacity
                  key={i}
                  style={styles.card}
                  onPress={() =>
                    this.props.navigation.navigate("Home", {
                      screen: "Detail",
                      params: { itemId: data.id },
                    })
                  }
                >
                  <Image
                    source={require("../../assets/pizza.jpg")}
                    style={styles.image}
                  ></Image>
                  <View style={styles.cardBody}>
                    <Text style={styles.cardTitle}>{data.name}</Text>
                    <Text style={styles.cardCategori}>Pizza</Text>
                    <View style={styles.cardDesc}>
                      <Text>Total Fat</Text>
                      <Text>{`${data.totalFat} g`}</Text>
                    </View>
                    <View style={styles.cardDesc}>
                      <Text>Calories</Text>
                      <Text>{`${data.calories} cal`}</Text>
                    </View>
                    <View style={styles.cardDesc}>
                      <Text>Cholesterol</Text>
                      <Text>{`${data.cholesterol} mg`}</Text>
                    </View>
                    <View style={styles.cardDesc}>
                      <Text>Sodium</Text>
                      <Text>{`${data.sodium} mg`}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
          <View style={styles.recommendationContainer}>
            <Text style={styles.recommendationText}>
              Based on the foods in this section
            </Text>
            <View style={styles.bottomLine} />
            <View style={styles.bottomContainer}>
              <View style={{ flexDirection: "row" }}>
                <Icon
                  name="silverware-fork-knife"
                  size={30}
                  color={colors.greyLight}
                />
                <Text style={styles.bottomTitle}>Pasta alla carbonara</Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  // console.log(item.fields.item_id);
                  this.props.add([
                    "57585b810e1c22587cac348c",
                    "Spaghetti Carbonara - 1 cup",
                    10.85,
                    336.4,
                    61.6,
                    318.93,
                  ]);
                }}
              >
                <Text style={styles.buttonAdd}>ADD</Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    foods: state.foodReducer.foodList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    delete: (key) => dispatch(deleteFood(key)),
    add: (food) => dispatch(addFood(food)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Favorite);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 12,
    backgroundColor: colors.darkBg,
    justifyContent: "flex-start",
    flexDirection: "column",
    fontFamily: "Roboto",
    paddingTop: 25,
  },
  headingContainer: {
    paddingTop: 35,
  },
  headingText: {
    color: "#FFF",
    fontSize: 42,
    fontWeight: "bold",
  },
  bottomLine: {
    paddingTop: 38,
    borderBottomColor: "#96A7AF",
    borderBottomWidth: 3,
    width: 299,
    marginLeft: 12,
  },
  card: {
    marginTop: 39,
    backgroundColor: colors.lightBg,
    marginHorizontal: 15,
    borderRadius: 10,
    overflow: "hidden",
    width: 200,
  },
  image: {
    width: "100%",
    height: 145,
  },
  cardBody: {
    margin: 15,
  },
  cardTitle: {
    color: "#4F4F4F",
    fontSize: 24,
    fontFamily: "Roboto",
    fontWeight: "bold",
  },
  cardCategori: {
    color: colors.greyLight,
    fontFamily: "Roboto",
    marginBottom: 25,
  },
  cardDesc: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  recommendationContainer: {
    marginTop: 32,
  },
  recommendationText: {
    color: colors.greyLight,
    marginLeft: 12,
  },
  bottomContainer: {
    marginTop: 16,
    marginHorizontal: 12,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bottomTitle: {
    fontSize: 13,
    color: colors.greyLight,
    marginLeft: 4,
  },
  buttonAdd: {
    fontSize: 13,
    color: "#fff",
    paddingLeft: 22,
    paddingHorizontal: 17,
    paddingVertical: 2,
    borderRadius: 12,
    paddingTop: 6,
    borderColor: colors.successButton,
    borderWidth: 2,
  },
});
