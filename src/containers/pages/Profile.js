import React, { Component } from "react";
import { Text, StyleSheet, View, Image } from "react-native";
import { colors } from "../../styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Profile extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.profileContainer}>
          <Image
            source={require("../../assets/pizza.jpg")}
            style={styles.image}
          ></Image>
          <Text style={styles.usernameText}> username </Text>
          <View style={styles.bottomLine} />
          <Text style={styles.recentFavsText}> Recently added foods </Text>
          <View style={styles.cardContainer}>
            <View style={{ flexDirection: "row" }}>
              <Image
                source={require("../../assets/pizza.jpg")}
                style={styles.foodImage}
              />
              <Text style={styles.foodName}>
                Pasta alla Carbonara {"\n"}1018 cal
              </Text>
            </View>
            <Icon name="chevron-right" size={30} color={colors.greyLight} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 50,
    backgroundColor: colors.darkBg,
    justifyContent: "flex-start",
    flexDirection: "column",
    fontFamily: "Roboto",
  },
  profileContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: 150,
    height: 150,
    borderRadius: 150 / 2,
    overflow: "hidden",
    borderWidth: 3,
    borderColor: colors.greyLight,
  },
  usernameText: {
    marginTop: 25,
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 24,
    color: colors.lightBg,
  },
  bottomLine: {
    marginTop: 36,
    borderBottomColor: "#96A7AF",
    borderBottomWidth: 3,
    width: "100%",
    marginVertical: 38,
  },
  recentFavsText: {
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 24,
    color: colors.lightBg,
  },
  cardContainer: {
    alignItems: "center",
    marginTop: 16,
    marginBottom: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    borderColor: colors.greyLight,
    borderWidth: 2,
    borderRadius: 12,
    padding: 12,
    width: "100%",
  },
  foodImage: {
    width: 50,
    height: 50,
    borderRadius: 150 / 2,
    overflow: "hidden",
    borderWidth: 3,
    borderColor: colors.greyLight,
  },
  foodName: {
    fontSize: 13,
    color: colors.greyLight,
    marginLeft: 4,
    marginTop: 6,
  },
});
