import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import {
  Home,
  Favorite,
  Setting,
  Profile,
  Detail,
  About,
  Login,
  Register,
} from "../pages";
import { StyleSheet } from "react-native";
import { colors } from "../../styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const Tabs = createBottomTabNavigator();
const LoginStack = createStackNavigator();
const HomeStack = createStackNavigator();
const SettingsStack = createStackNavigator();

const HomeStackScreen = () => (
  <HomeStack.Navigator headerMode="none">
    <HomeStack.Screen name="Home" component={Home} />
    <HomeStack.Screen name="Detail" component={Detail} />
  </HomeStack.Navigator>
);

const SettingsStackScreen = () => (
  <SettingsStack.Navigator headerMode="none">
    <SettingsStack.Screen name="Setting" component={Setting} />
    <SettingsStack.Screen name="About" component={About} />
  </SettingsStack.Navigator>
);

const AppScreens = () => (
  <Tabs.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === "Home") {
          iconName = focused ? "home" : "home-flood";
        } else if (route.name === "Favorite") {
          iconName = focused ? "heart" : "heart-outline";
        } else if (route.name === "Profile") {
          iconName = focused ? "account" : "account-outline";
        } else if (route.name === "Setting") {
          iconName = focused ? "hammer" : "cogs";
        }

        // You can return any component that you like here!
        return <Icon name={iconName} size={size} color={color} />;
      },
    })}
    tabBarOptions={{
      showLabel: false,
      style: styles.tabs,
      activeTintColor: colors.greenLight,
    }}
    initialRouteName="Home"
  >
    <Tabs.Screen name="Favorite" component={Favorite} />
    <Tabs.Screen name="Home" component={HomeStackScreen} />
    <Tabs.Screen name="Profile" component={Profile} />
    <Tabs.Screen name="Setting" component={SettingsStackScreen} />
  </Tabs.Navigator>
);
export default () => (
  <NavigationContainer>
    <LoginStack.Navigator headerMode="none">
      <LoginStack.Screen name="Login" component={Login} />
      <LoginStack.Screen name="Register" component={Register} />
      <LoginStack.Screen name="App" component={AppScreens} />
    </LoginStack.Navigator>
  </NavigationContainer>
);

const styles = StyleSheet.create({
  tabs: {
    backgroundColor: colors.greyDarker,
    shadowColor: colors.greyDarker,
    shadowOffset: { height: 14 },
    shadowOpacity: 100,
    shadowRadius: 2,
    elevation: 5,
    borderTopColor: "transparent",
    borderTopEndRadius: 25,
    borderTopStartRadius: 25,
    position: "absolute",
  },
});
