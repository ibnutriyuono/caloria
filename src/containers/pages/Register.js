import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";
import { colors } from "../../styles";

export default class Register extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.kotak}>
          <Text> </Text>
        </View>
        <Text style={styles.wellcome}>Hello!</Text>
        <Text style={styles.textSignIn}>Lets introduce</Text>
        <View style={styles.inputContainer}>
          <Icon style={styles.personIcon} name="account" size={41} />
          <TextInput
            style={styles.textInput}
            placeholder="Username"
          ></TextInput>
        </View>
        <View style={styles.inputContainer}>
          <Icon style={styles.personIcon} name="email" size={41} />
          <TextInput style={styles.textInput} placeholder="Email"></TextInput>
        </View>
        <View style={styles.inputContainer}>
          <Icon style={styles.lockIcon} name="lock" size={41} />
          <TextInput
            style={styles.textInput}
            secureTextEntry={true}
            placeholder="Password"
          ></TextInput>
        </View>
        <View style={styles.inputContainer}>
          <Icon style={styles.lockIcon} name="lock" size={41} />
          <TextInput
            style={styles.textInput}
            secureTextEntry={true}
            placeholder="Confirm Password"
          ></TextInput>
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity
            style={styles.btnSignUp}
            onPress={() => this.props.navigation.push("Login")}
          >
            <Icon
              style={styles.signUpText}
              name="arrow-left"
              size={16}
              color="#ffffff"
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnSignIn}
            onPress={() => this.props.navigation.push("App")}
          >
            <Text style={styles.signInText}>Sign up</Text>
            <Icon name="arrow-right" size={16} color="#ffffff" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 12,
    backgroundColor: colors.darkBg,
    justifyContent: "flex-end",
    flexDirection: "column",
    fontFamily: "Roboto",
  },
  kotak: {
    backgroundColor: colors.greenLight,
    width: 45,
    height: 45,
    borderRadius: 10,
    marginBottom: 15,
  },
  wellcome: {
    color: colors.lightBg,
    fontSize: 42,
    fontWeight: "bold",
  },
  textSignIn: {
    color: colors.greyLight,
    fontSize: 24,
  },
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 20,
  },
  textInput: {
    paddingLeft: 15,
    fontSize: 18,
    color: colors.greyLight,
    width: 320,
    height: 54,
    borderBottomColor: "white",
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 4,
  },
  personIcon: {
    backgroundColor: colors.yellowDark,
    color: colors.yellowLight,
    marginTop: 6,
  },
  lockIcon: {
    backgroundColor: colors.redDark,
    color: colors.redLight,
    marginTop: 6,
  },
  btnSignIn: {
    backgroundColor: colors.greenLight,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    flexDirection: "row",
    width: 270,
  },
  signInText: {
    color: colors.lightBg,
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 16,
    marginRight: 5,
  },
  btnSignUp: {
    backgroundColor: colors.greenDark,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    flexDirection: "row",
    width: 58,
    marginRight: 10,
  },
  signUpText: {
    color: colors.greenLight,
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 16,
  },
  btnContainer: {
    marginTop: 35,
    flexDirection: "row",
    justifyContent: "center",
  },
});
