import React, { Component } from "react";
import { Text, StyleSheet, View, Image, FlatList } from "react-native";
import { colors } from "../../styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { addFood } from "../../config/actions";

class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    // console.log(this.props.route.params.itemId);
    this.getDishById();
  }

  getDishById = async () => {
    const { itemId } = this.props.route.params;
    console.log(itemId);
    const response = await fetch(`https://api.nutritionix.com/v1_1/search`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        appId: "96f3b741",
        appKey: "0cf72cc7447fdf0afeaa2a108fa2926d",
        fields: [
          "item_name",
          "brand_name",
          "item_id",
          "nf_calories",
          "nf_sodium",
          "nf_total_fat",
          "nf_cholesterol",
        ],
        filters: {
          item_id: `${itemId}`,
        },
      }),
    });
    const json = await response.json();
    // console.log(json);
    this.setState((state) => ({
      data: [...state.data, ...json.hits],
      loading: false,
    }));
  };
  render() {
    return (
      <FlatList
        data={this.state.data}
        keyExtractor={(x, i) => i.toString()}
        renderItem={({ item }) => (
          <View style={styles.container}>
            <View style={styles.imageContainer}>
              <Image
                source={require("../../assets/pizza.jpg")}
                style={styles.image}
              ></Image>
            </View>
            <View style={styles.cardContainer}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginTop: 25,
                }}
              >
                <Text
                  style={styles.headingText}
                >{`${item.fields.item_name}`}</Text>
                <TouchableOpacity
                  onPress={() => {
                    // console.log(item.fields.item_id);
                    this.props.add([
                      item.fields.item_id,
                      item.fields.item_name,
                      item.fields.nf_total_fat,
                      item.fields.nf_calories,
                      item.fields.nf_cholesterol,
                      item.fields.nf_sodium,
                    ]);
                  }}
                >
                  <Icon name="heart" size={30} color={colors.greyLight} />
                </TouchableOpacity>
              </View>
              <Text style={styles.categoryText}>Pizza</Text>
              <View style={styles.bottomLine} />

              <View style={styles.nutritionFactsContainer}>
                <Text style={styles.nutritionFactsText}>Nutrition Facts</Text>
                <View style={{ marginTop: 21 }}>
                  <View style={styles.textContainer}>
                    <Text style={styles.nutrientsText}>Total Fat</Text>
                    <Text
                      style={styles.nutrientsValue}
                    >{`${item.fields.nf_total_fat} g`}</Text>
                  </View>
                  <View style={styles.textContainer}>
                    <Text style={styles.nutrientsText}>Calories</Text>
                    <Text
                      style={styles.nutrientsValue}
                    >{`${item.fields.nf_calories} cal`}</Text>
                  </View>
                  <View style={styles.textContainer}>
                    <Text style={styles.nutrientsText}>Cholesterol</Text>
                    <Text
                      style={styles.nutrientsValue}
                    >{`${item.fields.nf_cholesterol} mg`}</Text>
                  </View>
                  <View style={styles.textContainer}>
                    <Text style={styles.nutrientsText}>Sodium</Text>
                    <Text
                      style={styles.nutrientsValue}
                    >{`${item.fields.nf_sodium} mg`}</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        )}
      />
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    foods: state.foodReducer.foodList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    add: (food) => dispatch(addFood(food)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Detail);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.darkBg,
    justifyContent: "center",
    flexDirection: "column",
    fontFamily: "Roboto",
  },
  imageContainer: { height: 250, zIndex: 1 },
  image: {
    width: "100%",
    height: 420,
  },
  cardContainer: {
    flex: 3,
    backgroundColor: colors.lightBg,
    borderTopStartRadius: 25,
    borderTopEndRadius: 25,
    zIndex: 2,
    paddingHorizontal: 20,
  },
  headingText: {
    fontFamily: "Roboto",
    fontWeight: "bold",
    color: colors.greyDarker,
    fontSize: 26,
    width: "80%",
  },
  categoryText: {
    fontFamily: "Roboto",
    fontWeight: "bold",
    color: colors.greyLight,
    fontSize: 24,
  },
  bottomLine: {
    paddingTop: 14,
    borderBottomColor: "#96A7AF",
    borderBottomWidth: 3,
    width: 299,
  },
  nutritionFactsContainer: { flexDirection: "column" },
  nutritionFactsText: {
    marginTop: 21,
    color: colors.greyDarker,
    fontSize: 24,
    fontFamily: "Roboto",
  },
  textContainer: { flexDirection: "row", justifyContent: "space-between" },
  nutrientsText: {
    color: colors.greyDarker,
    fontSize: 24,
    fontFamily: "Roboto",
  },
  nutrientsValue: {
    color: colors.greyDarker,
    fontSize: 24,
    fontFamily: "Roboto",
  },
});
