// const increment = () => {
//   return {
//     type: "INCREMENT",
//   };
// };

// const decrement = () => {
//   return {
//     type: "DECREMENT",
//   };
// };

import { ADD_FOOD, DELETE_FOOD, LOGIN, LOGOUT } from "./types";
export const addFood = (food) => ({
  type: ADD_FOOD,
  data: food,
});

export const deleteFood = (key) => ({
  type: DELETE_FOOD,
  key: key,
});
