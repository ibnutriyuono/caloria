// const counter = (state = 0, action) => {
//   switch (action.type) {
//     case "INCREMENT":
//       return state + 1;
//     case "DECREMENT":
//       return state - 1;
//   }
// };

import { ADD_FOOD, DELETE_FOOD, LOGIN, LOGOUT } from "../actions/types";

const initialState = {
  foodList: [],
  isLoggedIn: false,
};

const foodReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_FOOD:
      return {
        ...state,
        foodList: state.foodList.concat({
          key: Math.random(),
          id: action.data[0],
          name: action.data[1],
          totalFat: action.data[2],
          calories: action.data[3],
          cholesterol: action.data[4],
          sodium: action.data[5],
        }),
      };
    case DELETE_FOOD:
      return {
        ...state,
        foodList: state.foodList.filter((item) => item.key !== action.key),
      };
    default:
      return state;
  }
};

export default foodReducer;
